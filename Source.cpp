#include<stdlib.h>
#include<stdio.h>

int main() {

	int matrixsize = 0;
	printf("Enter square matrix size: -->> (You have to enter 2(two) or 3(three) !!)  ");
	scanf("%d", &matrixsize);


	if (matrixsize == 3)
	{
		//3lü matrix
		int matrix[9];

		for (int i = 0; i < 9; i++)
		{
			printf("Matrix %d.Indexini Gir: ", i + 1);
			scanf("%d", &matrix[i]);
			printf("\n");
		}

		//hata sayısı
		int hata = 0;

		//1.kural
		for (int i = 0; i < 9; i++)
		{
			if (matrix[i] == 2)
			{
				if (matrix[i + 3] == 1)
				{


					if (i + 3 == 8) {
						printf("NOT MATCH[%d][%d] : %d", 2, 2, 1);
					}
					if (i + 3 == 7) {
						printf("NOT MATCH[%d][%d] : %d", 1, 2, 1);
					}
					if (i + 3 == 6) {
						printf("NOT MATCH[%d][%d] : %d", 0, 2, 1);
					}
					if (i + 3 == 5) {
						printf("NOT MATCH[%d][%d] : %d", 2, 1, 1);
					}
					if (i + 3 == 4) {
						printf("NOT MATCH[%d][%d] : %d", 1, 1, 1);
					}
					if (i + 3 == 3) {
						printf("NOT MATCH[%d][%d] : %d", 0, 1, 1);
					}
					printf("\n");
					printf("Reason : Rule 1.\n");
					hata++;
				}
			}
		}

		//2.kural
		for (int i = 0; i < 9; i++)
		{
			if (matrix[i] == 2)
			{
				if (matrix[i + 1] == 3)
				{
					if (i + 1 == 8) {
						printf("NOT MATCH[%d][%d] : %d", 2, 2, 3);
					}
					if (i + 1 == 7) {
						printf("NOT MATCH[%d][%d] : %d", 1, 2, 3);
					}
					if (i + 1 == 6) {
						printf("NOT MATCH[%d][%d] : %d", 0, 2, 3);
					}
					if (i + 1 == 5) {
						printf("NOT MATCH[%d][%d] : %d", 2, 1, 3);
					}
					if (i + 1 == 4) {
						printf("NOT MATCH[%d][%d] : %d", 1, 1, 3);
					}
					if (i + 1 == 3) {
						printf("NOT MATCH[%d][%d] : %d", 0, 1, 3);
					}
					if (i + 1 == 2) {
						printf("NOT MATCH[%d][%d] : %d", 2, 0, 3);
					}
					if (i + 1 == 1) {
						printf("NOT MATCH[%d][%d] : %d", 1, 0, 3);
					}
					printf("\n");
					printf("Reason : Rule 2.\n");
					hata++;
				}
			}
		}

		//3.kural
		for (int i = 0; i < 9; i++)
		{
			if (matrix[i] == 3)
			{
				if (matrix[i + 3] == 4)
				{
					if (i + 3 == 8) {
						printf("NOT MATCH[%d][%d] : %d", 2, 2, 4);
					}
					if (i + 3 == 7) {
						printf("NOT MATCH[%d][%d] : %d", 1, 2, 4);
					}
					if (i + 3 == 6) {
						printf("NOT MATCH[%d][%d] : %d", 0, 2, 4);
					}
					if (i + 3 == 5) {
						printf("NOT MATCH[%d][%d] : %d", 2, 1, 4);
					}
					if (i + 3 == 4) {
						printf("NOT MATCH[%d][%d] : %d", 1, 1, 4);
					}
					if (i + 3 == 3) {
						printf("NOT MATCH[%d][%d] : %d", 0, 1, 4);
					}
					printf("\n");
					printf("Reason : Rule 3.\n");
					hata++;
				}
			}

		}

		//4.kural
		for (int i = 0; i < 9; i++)
		{
			if (matrix[i] == 1)
			{
				if (matrix[i + 1] == 4)
				{
					if (i + 1 == 8) {
						printf("NOT MATCH[%d][%d] : %d", 2, 2, 4);
					}
					if (i + 1 == 7) {
						printf("NOT MATCH[%d][%d] : %d", 1, 2, 4);
					}
					if (i + 1 == 6) {
						printf("NOT MATCH[%d][%d] : %d", 0, 2, 4);
					}
					if (i + 1 == 5) {
						printf("NOT MATCH[%d][%d] : %d", 2, 1, 4);
					}
					if (i + 1 == 4) {
						printf("NOT MATCH[%d][%d] : %d", 1, 1, 4);
					}
					if (i + 1 == 3) {
						printf("NOT MATCH[%d][%d] : %d", 0, 1, 4);
					}
					if (i + 1 == 2) {
						printf("NOT MATCH[%d][%d] : %d", 2, 0, 4);
					}
					if (i + 1 == 1) {
						printf("NOT MATCH[%d][%d] : %d", 1, 0, 4);
					}
					printf("\n");
					printf("Reason : Rule 4.\n");
					hata++;
				}
			}

		}


		if (hata == 0)
		{
			printf("ACCEPTED\n");
		}
	}


	if (matrixsize == 2)
	{
		int matrix[9];

		for (int i = 0; i < 4; i++)
		{
			printf("Matrix %d.Indexini Gir: ", i + 1);
			scanf("%d", &matrix[i]);
			printf("\n");
		}

		int hata = 0;

		//1.kural
		for (int i = 0; i < 4; i++)
		{
			if (matrix[i] == 2)
			{
				if (matrix[i + 3] == 1)
				{
					
					if (i + 3 == 3) {
						printf("NOT MATCH[%d][%d] : %d", 1, 1, 1);
					}
					printf("\n");
					printf("Reason : Rule 1.\n");
					hata++;
				}
			}
		}

		//2.kural
		for (int i = 0; i < 4; i++)
		{
			if (matrix[i] == 2)
			{
				if (matrix[i + 1] == 3)
				{
					if (i + 1 == 3) {
						printf("NOT MATCH[%d][%d] : %d", 1, 1, 3);
					}
					if (i + 1 == 2) {
						printf("NOT MATCH[%d][%d] : %d", 0, 1, 3);
					}
					if (i + 1 == 1) {
						printf("NOT MATCH[%d][%d] : %d", 1, 0, 3);
					}
					printf("\n");
					printf("Reason : Rule 2.\n");
					hata++;
				}
			}
		}

		//3.kural
		for (int i = 0; i < 4; i++)
		{
			if (matrix[i] == 3)
			{
				if (matrix[i + 3] == 4)
				{
				
					if (i + 3 == 3) {
						printf("NOT MATCH[%d][%d] : %d", 1, 1, 4);
					}
					printf("\n");
					printf("Reason : Rule 3.\n");
					hata++;
				}
			}

		}

		//4.kural
		for (int i = 0; i < 4; i++)
		{
			if (matrix[i] == 1)
			{
				if (matrix[i + 1] == 4)
				{
					if (i + 1 == 3) {
						printf("NOT MATCH[%d][%d] : %d", 0, 1, 4);
					}
					if (i + 1 == 2) {
						printf("NOT MATCH[%d][%d] : %d", 0, 1, 4);
					}
					if (i + 1 == 1) {
						printf("NOT MATCH[%d][%d] : %d", 1, 0, 4);
					}
					printf("\n");
					printf("Reason : Rule 4.\n");
					hata++;
				}
			}

		}

		if (hata == 0)
		{
			printf("ACCEPTED\n");
		}


	}

	system("pause");
	return 0;
}